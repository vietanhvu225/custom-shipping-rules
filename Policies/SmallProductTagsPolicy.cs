﻿using Sitecore.Commerce.Core;
using System.Collections.Generic;

namespace Feature.Shipping.Engine.Policies
{
    public class SmallProductTagsPolicy : Policy
    {
        public SmallProductTagsPolicy()
        {
            this.TagList = new List<string>()
            {
                "smallproduct"
            };
        }
        public List<string> TagList { get; private set; } = new List<string>();
    }
}
