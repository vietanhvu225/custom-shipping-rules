﻿using Sitecore.Commerce.Core;
using System.Collections.Generic;

namespace Feature.Shipping.Engine.Policies
{
    public class FreeShippingItemTagsPolicy : Policy
    {
        public FreeShippingItemTagsPolicy()
        {
            this.TagList = new List<string>()
            {
                "massagechairs",
                "massagesofas",
                "freeshipping"
            };
        }
        public List<string> TagList { get; private set; } = new List<string>();
    }
}
