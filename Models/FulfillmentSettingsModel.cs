﻿namespace Sitecore.Commerce.Plugin.Models
{
    using Sitecore.Commerce.Core;
    using Sitecore.Services.Core.Model;
    using System.Collections.Generic;

    /// <inheritdoc />
    /// <summary>
    /// Defines a model
    /// </summary>
    /// <seealso cref="T:Sitecore.Commerce.Core.Model" />
    public class FulfillmentSettingsModel : Model
    {
        /// <inheritdoc />
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Sitecore.Commerce.Plugin.Sample.SampleModel" /> class.
        /// </summary>
        public FulfillmentSettingsModel()
        {
            this.Zone1 = new List<string>();
            this.FixPriceForZone1 = string.Empty;
            this.Zone2 = new List<string>();
            this.FixPriceForZone2 = string.Empty;
            this.Zone3 = new List<string>();
            this.FixPriceForZone3 = string.Empty;
            this.SubdivisionFreeShipping = new List<string>();
            this.SmallProductsShippingCost = string.Empty;
            this.FreeShippingForTotalAmount = string.Empty;
            this.FixedMinFee = string.Empty;
            this.AdditionalDeliveryPriceList = string.Empty;
        }

        /// <summary>
        /// Gets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public List<string> Zone1 { get; set; }
        public string FixPriceForZone1 { get; set; }
        public List<string> Zone2 { get; set; }
        public string FixPriceForZone2 { get; set; }
        public List<string> Zone3 { get; set; }
        public string FixPriceForZone3 { get; set; }
        public List<string> SubdivisionFreeShipping { get; set; }
        public string FreeShippingForTotalAmount { get; set; }
        public string SmallProductsShippingCost { get; set; }
        public string FixedMinFee { get; set; }
        public string AdditionalDeliveryPriceList { get; set; }
    }
}
