﻿using Feature.Shipping.Engine.Models;
using Sitecore.Commerce.Core;
using Sitecore.Commerce.Plugin.Carts;
using Sitecore.Commerce.Plugin.Fulfillment;
using Sitecore.Commerce.Plugin.Management;
using Sitecore.Commerce.Plugin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Feature.Shipping.Engine.Helpers
{
    public static class FulfillmentHelpers
    {
        public static CustomerFulfillmentInformationModel GetCustomerFulfillmentInformation(CustomerFulfillmentInformationModel customerFulfillmentInformationModel, PhysicalFulfillmentComponent physicalFulfillmentComponent)
        {
            // Get customer's submited infomation
            //PhysicalFulfillmentComponent physicalFulfillmentComponent = (PhysicalFulfillmentComponent)arg.GetComponent<PhysicalFulfillmentComponent>();
            if (physicalFulfillmentComponent != null)
            {
                var shippingParty = physicalFulfillmentComponent.ShippingParty;
                if (shippingParty != null)
                {
                    customerFulfillmentInformationModel.City = shippingParty.City;
                    customerFulfillmentInformationModel.Country = shippingParty.Country;
                    customerFulfillmentInformationModel.CountryCode = shippingParty.CountryCode;
                    customerFulfillmentInformationModel.State = shippingParty.State;
                    customerFulfillmentInformationModel.StateCode = shippingParty.StateCode;
                }
            }
            
            return customerFulfillmentInformationModel;
        }
        public static async Task<FulfillmentSettingsModel> GetFulfillmentSettings(FulfillmentSettingsModel fulfillmentSettingsModel, CommerceContext commerceContext)
        {
            using (var connectionManager = new SitecoreConnectionManager())
            {
                var path = $"/sitecore/Commerce/Commerce Control Panel/Storefront Settings/Storefronts/HabitatHome/Fulfillment Configuration";
                var fulfillmentSettingsItem = await connectionManager.GetItemByPathAsync(commerceContext, path);

                var zone1Subdivision = fulfillmentSettingsItem.GetFieldValue("Zone 1").ToString().Split('|');
                var zone2Subdivision = fulfillmentSettingsItem.GetFieldValue("Zone 2").ToString().Split('|');
                var zone3Subdivision = fulfillmentSettingsItem.GetFieldValue("Zone 3").ToString().Split('|');

                var subdivisionFreeShipping = fulfillmentSettingsItem.GetFieldValue("Subdivision free shipping").ToString().Split('|');
                var freeShippingForTotalAmount = fulfillmentSettingsItem.GetFieldValue("Free shipping for order total amount is over")?.ToString();

                // Internal shipping
                var smallProductsShippingCost = fulfillmentSettingsItem.GetFieldValue("Small products shipping cost")?.ToString();

                var fixedFeeWhenTotalAmount = fulfillmentSettingsItem.GetFieldValue("Total amount of the order is less than xxx - fixed delivery fee is yyy")?.ToString();
                var additionalDeliveryPriceList = fulfillmentSettingsItem.GetFieldValue("Additional delivery price list")?.ToString();

                if (zone1Subdivision.Count() > 0)
                {
                    await GetSubdivisionCode(fulfillmentSettingsModel.Zone1, zone1Subdivision, commerceContext, connectionManager);
                    var zone1Fee = fulfillmentSettingsItem.GetFieldValue("Fixed price for zone 1").ToString();
                    fulfillmentSettingsModel.FixPriceForZone1 = zone1Fee;
                }

                if (zone2Subdivision.Count() > 0)
                {
                    await GetSubdivisionCode(fulfillmentSettingsModel.Zone2, zone2Subdivision, commerceContext, connectionManager);
                    var zone2Fee = fulfillmentSettingsItem.GetFieldValue("Fixed price for zone 2").ToString();
                    fulfillmentSettingsModel.FixPriceForZone1 = zone2Fee;
                }

                if (zone3Subdivision.Count() > 0)
                {
                    await GetSubdivisionCode(fulfillmentSettingsModel.Zone3, zone3Subdivision, commerceContext, connectionManager);
                    var zone3Fee = fulfillmentSettingsItem.GetFieldValue("Fixed price for zone 3").ToString();
                    fulfillmentSettingsModel.FixPriceForZone1 = zone3Fee;
                }

                if (subdivisionFreeShipping.Count() > 0)
                {
                    await GetSubdivisionCode(fulfillmentSettingsModel.SubdivisionFreeShipping, subdivisionFreeShipping, commerceContext, connectionManager);
                }

                fulfillmentSettingsModel.FreeShippingForTotalAmount = freeShippingForTotalAmount;
                fulfillmentSettingsModel.SmallProductsShippingCost = smallProductsShippingCost;
                fulfillmentSettingsModel.FixedMinFee = fixedFeeWhenTotalAmount;
                fulfillmentSettingsModel.AdditionalDeliveryPriceList = additionalDeliveryPriceList;
            }
            return fulfillmentSettingsModel;
        }

        public static async Task<List<string>> GetSubdivisionCode(List<string> model, string[] subdivisions, CommerceContext commerceContext, SitecoreConnectionManager connectionManager)
        {
            foreach (var subdivision in subdivisions)
            {
                var subdivisionItemModel = await connectionManager.GetItemByIdAsync(commerceContext, subdivision);
                var subdivisionCode = subdivisionItemModel.GetFieldValue("Code").ToString();
                model.Add(subdivisionCode);
            }
            return model;
        }
    }
}
