﻿using Feature.Shipping.Engine.Policies;
using Sitecore.Commerce.Core;
using Sitecore.Commerce.Plugin.Catalog;
using Sitecore.Framework.Pipelines;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Feature.Shipping.Engine.Pipelines.Blocks
{
    [PipelineDisplayName("Availability.EnsureSellableItemIsExternalShippingPoliciesBlock")]
    public class EnsureSellableItemIsFreeShippingPoliciesBlock : PolicyTriggerConditionalPipelineBlock<SellableItem, SellableItem>
    {
        /// <inheritdoc />
        /// <summary>Gets the should not run policy trigger.</summary>
        /// <value>The should not run policy trigger.</value>
        public override string ShouldNotRunPolicyTrigger
        {
            get
            {
                return "IgnoreFreeShipping";
            }
        }

        /// <summary>The execute.</summary>
        /// <param name="arg">The argument.</param>
        /// <param name="context">The context.</param>
        /// <returns>
        /// The <see cref="T:Sitecore.Commerce.Plugin.Catalog.SellableItem" />.
        /// </returns>
        public override Task<SellableItem> Run(
          SellableItem arg,
          CommercePipelineExecutionContext context)
        {
            if (arg == null)
                return Task.FromResult<SellableItem>((SellableItem)null);
            bool flag = false;
            FreeShippingItemTagsPolicy policy = context.GetPolicy<FreeShippingItemTagsPolicy>();
            foreach (Tag tag in (IEnumerable<Tag>)arg.Tags)
            {
                if (policy.TagList.Contains<string>(tag.Name, (IEqualityComparer<string>)StringComparer.OrdinalIgnoreCase))
                    flag = true;
            }
            if (!flag)
                return Task.FromResult<SellableItem>(arg);

            if (!arg.HasPolicy<FreeShippingItemTagsPolicy>())
                arg.SetPolicy((Policy)new FreeShippingItemTagsPolicy());
            if (!arg.HasComponent<ItemVariationsComponent>())
                return Task.FromResult<SellableItem>(arg);
            foreach (Component component in arg.GetComponent<ItemVariationsComponent>().GetComponents<ItemVariationComponent>())
                component.SetPolicy((Policy)new FreeShippingItemTagsPolicy());
            return Task.FromResult<SellableItem>(arg);
        }
    }
}
