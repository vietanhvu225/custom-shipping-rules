﻿using Feature.Shipping.Engine.Helpers;
using Feature.Shipping.Engine.Models;
using Feature.Shipping.Engine.Policies;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Sitecore.Commerce.Core;
using Sitecore.Commerce.Plugin.Carts;
using Sitecore.Commerce.Plugin.Fulfillment;
using Sitecore.Commerce.Plugin.Management;
using Sitecore.Commerce.Plugin.Models;
using Sitecore.Commerce.Plugin.Pricing;
using Sitecore.Framework.Conditions;
using Sitecore.Framework.Pipelines;
using Sitecore.Services.Core.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace Feature.Shipping.Engine.Pipelines.Blocks
{
    [PipelineDisplayName("Fulfillment.block.CalculateCartFulfillmentExtendedBlock")]
    public class CalculateCartFulfillmentExtendedBlock : PipelineBlock<Cart, Cart, CommercePipelineExecutionContext>
    {
        public CalculateCartFulfillmentExtendedBlock()
        {

        }
        /// <summary>The execute.</summary>
        /// <param name="arg">The argument.</param>
        /// <param name="context">The context.</param>
        /// <returns>
        /// The <see cref="T:Sitecore.Commerce.Plugin.Carts.Cart" />.
        /// </returns>
        public override async Task<Cart> Run(Cart arg, CommercePipelineExecutionContext context)
        {
            Condition.Requires<Cart>(arg).IsNotNull<Cart>(this.Name + ": The cart cannot be null.");
            if (!arg.HasComponent<FulfillmentComponent>())
                return arg;
            FulfillmentComponent fulfillmentComponent = (FulfillmentComponent)arg.GetComponent<FulfillmentComponent>();

            if (!arg.Lines.Any<CartLineComponent>())
            {
                arg.RemoveComponents(new Component[1] { (Component)fulfillmentComponent });
                ((IEnumerable<AwardedAdjustment>)arg.Adjustments).Where<AwardedAdjustment>((Func<AwardedAdjustment, bool>)(a =>
                {
                    if (!string.IsNullOrEmpty(((Adjustment)a).Name) && ((Adjustment)a).Name.Equals("FulfillmentFee", StringComparison.OrdinalIgnoreCase) && !string.IsNullOrEmpty(a.AdjustmentType))
                        return a.AdjustmentType.Equals(((KnownCartAdjustmentTypesPolicy)context.GetPolicy<KnownCartAdjustmentTypesPolicy>()).Fulfillment, StringComparison.OrdinalIgnoreCase);
                    return false;
                })).ToList<AwardedAdjustment>().ForEach((Action<AwardedAdjustment>)(a => ((ICollection<AwardedAdjustment>)arg.Adjustments).Remove(a)));
                return arg;
            }

            string currency = context.CommerceContext.CurrentCurrency();
            GlobalPhysicalFulfillmentPolicy policy = (GlobalPhysicalFulfillmentPolicy)context.GetPolicy<GlobalPhysicalFulfillmentPolicy>();
            Decimal amount = policy.DefaultItemFulfillmentFee.Amount;

            if (((IEnumerable<Money>)policy.DefaultCartFulfillmentFees).Any<Money>((Func<Money, bool>)(p => p.CurrencyCode.Equals(currency, StringComparison.OrdinalIgnoreCase))))
                amount = ((IEnumerable<Money>)policy.DefaultCartFulfillmentFees).First<Money>((Func<Money, bool>)(p => p.CurrencyCode.Equals(currency, StringComparison.OrdinalIgnoreCase))).Amount;
            ((DefaultPipelineExecutionContext)context).Logger.LogDebug(string.Format("{0} - Default Fulfillment Fee:{1} {2}", (object)this.Name, (object)currency, (object)amount));

            if (fulfillmentComponent is ElectronicFulfillmentComponent || fulfillmentComponent is SplitFulfillmentComponent)
                return arg;
            ((DefaultPipelineExecutionContext)context).Logger.LogDebug(this.Name + " - Fulfillment Method:" + fulfillmentComponent.FulfillmentMethod.Name);

            if (policy.FulfillmentFees.Any<FulfillmentFee>((Func<FulfillmentFee, bool>)(p =>
            {
                if (p.Name.Equals(fulfillmentComponent.FulfillmentMethod.Name, StringComparison.OrdinalIgnoreCase))
                    return p.Fee.CurrencyCode.Equals(context.CommerceContext.CurrentCurrency(), StringComparison.OrdinalIgnoreCase);
                return false;
            })))
            {
                // Get subtotal amount
                var subTotalAmount = arg.Totals.SubTotal.Amount;

                // Get customer's fulfillment information
                var customerFulfillmentInformationModel = new CustomerFulfillmentInformationModel();

                PhysicalFulfillmentComponent physicalFulfillmentComponent = null;
                if (arg.HasComponent<PhysicalFulfillmentComponent>())
                {
                    physicalFulfillmentComponent = (PhysicalFulfillmentComponent)arg.GetComponent<PhysicalFulfillmentComponent>();
                    FulfillmentHelpers.GetCustomerFulfillmentInformation(customerFulfillmentInformationModel, physicalFulfillmentComponent);

                    // Get fulfillment settings
                    var fulfillmentSettingsModel = new FulfillmentSettingsModel();
                    await FulfillmentHelpers.GetFulfillmentSettings(fulfillmentSettingsModel, context.CommerceContext);

                    // Calculate fulfillment amount
                    if (fulfillmentSettingsModel.SubdivisionFreeShipping.Contains(customerFulfillmentInformationModel.StateCode) ||
                        subTotalAmount > Convert.ToDecimal(fulfillmentSettingsModel.FreeShippingForTotalAmount) ||
                        physicalFulfillmentComponent.HasPolicy<FreeShippingItemTagsPolicy>())
                    {
                        amount = 0;
                    }
                    else if (!string.IsNullOrWhiteSpace(fulfillmentSettingsModel.FixedMinFee))
                    {
                        var fixedMinFeeCollection = HttpUtility.ParseQueryString(fulfillmentSettingsModel.FixedMinFee);
                        foreach (string key in fixedMinFeeCollection)
                        {
                            var fixShippingAmount = fixedMinFeeCollection[key];
                            if (subTotalAmount < Convert.ToDecimal(key))
                            {
                                amount = Convert.ToDecimal(fixShippingAmount);
                                break;
                            }
                        }
                    }
                    else if (physicalFulfillmentComponent.HasPolicy<SmallProductTagsPolicy>())
                    {
                        amount = Convert.ToDecimal(fulfillmentSettingsModel.SmallProductsShippingCost);
                    }
                }
                else
                {
                    amount = policy.FulfillmentFees.First<FulfillmentFee>((Func<FulfillmentFee, bool>)(p => p.Name.Equals(fulfillmentComponent.FulfillmentMethod.Name, StringComparison.OrdinalIgnoreCase))).Fee.Amount;
                }

                ((DefaultPipelineExecutionContext)context).Logger.LogDebug(this.Name + " - Specific fee:" + fulfillmentComponent.FulfillmentMethod.Name);
            }

            IList<AwardedAdjustment> adjustments = arg.Adjustments;
            CartLevelAwardedAdjustment awardedAdjustment = new CartLevelAwardedAdjustment();
            ((Adjustment)awardedAdjustment).Name = "FulfillmentFee";
            ((Adjustment)awardedAdjustment).DisplayName = "FulfillmentFee";
            ((AwardedAdjustment)awardedAdjustment).Adjustment = new Money(currency, amount);
            ((AwardedAdjustment)awardedAdjustment).AdjustmentType = ((KnownCartAdjustmentTypesPolicy)context.GetPolicy<KnownCartAdjustmentTypesPolicy>()).Fulfillment;
            ((AwardedAdjustment)awardedAdjustment).IsTaxable = false;
            ((AwardedAdjustment)awardedAdjustment).AwardingBlock = this.Name;
            ((ICollection<AwardedAdjustment>)adjustments).Add((AwardedAdjustment)awardedAdjustment);

            context.Logger.LogInformation($"{this.Name}Fulfillment Fee is {awardedAdjustment.Adjustment.Amount}");
            return arg;
        }
    }
}
