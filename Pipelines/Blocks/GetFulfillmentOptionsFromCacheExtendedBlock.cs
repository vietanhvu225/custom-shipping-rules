﻿using Sitecore.Commerce.Core;
using Sitecore.Commerce.Plugin.Fulfillment;
using Sitecore.Commerce.Plugin.Management;
using Sitecore.Framework.Pipelines;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Feature.Shipping.Engine.Pipelines.Blocks
{
    [PipelineDisplayName("Management.block.GetFulfillmentOptionsFromCacheExtended")]
    public class GetFulfillmentOptionsFromCacheExtendedBlock : PipelineBlock<string, IEnumerable<FulfillmentOption>, CommercePipelineExecutionContext>
    {
        private readonly CommerceCommander _commander;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Sitecore.Commerce.Plugin.Management.GetFulfillmentOptionsFromCacheBlock" /> class.
        /// </summary>
        /// <param name="commander">The commander.</param>
        /// <inheritdoc />
        public GetFulfillmentOptionsFromCacheExtendedBlock(CommerceCommander commander)
          : base((string)null)
        {
            this._commander = commander;
        }

        /// <summary>Runs the specified argument.</summary>
        /// <param name="arg">The argument.</param>
        /// <param name="context">The context.</param>
        /// <returns>
        /// The list of <see cref="T:Sitecore.Commerce.Plugin.Fulfillment.FulfillmentOption" />
        /// </returns>
        public override async Task<IEnumerable<FulfillmentOption>> Run(
          string arg,
          CommercePipelineExecutionContext context)
        {
            if (!string.IsNullOrEmpty(arg))
                context.CommerceContext.AddUniqueObjectByType((object)new KeyValuePair<string, string>("itemPath", arg));
            string cacheEntryKey = "fulfillmentoptions|" + context.CommerceContext.CurrentShopName() + "|" + context.CommerceContext.CurrentLanguage();
            ManagementCachePolicy policy = context.GetPolicy<ManagementCachePolicy>();
            if (!policy.AllowCaching)
                return Enumerable.Empty<FulfillmentOption>();
            return await this._commander.GetCacheEntry<IEnumerable<FulfillmentOption>>(context.CommerceContext, policy.FulfillmentCacheName, cacheEntryKey).ConfigureAwait(false) ?? Enumerable.Empty<FulfillmentOption>();
        }
    }
}
