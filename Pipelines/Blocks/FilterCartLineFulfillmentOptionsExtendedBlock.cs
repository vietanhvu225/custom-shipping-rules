﻿using Microsoft.Extensions.Logging;
using Sitecore.Commerce.Core;
using Sitecore.Commerce.Plugin.Carts;
using Sitecore.Commerce.Plugin.Fulfillment;
using Sitecore.Framework.Conditions;
using Sitecore.Framework.Pipelines;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Feature.Shipping.Engine.Pipelines.Blocks
{
    [PipelineDisplayName("Fulfillment.block.FilterCartLineFulfillmentOptionsExtendedBlock")]
    public class FilterCartLineFulfillmentOptionsExtendedBlock : PipelineBlock<CartLineArgument, IEnumerable<FulfillmentOption>, CommercePipelineExecutionContext>
    {
        private readonly IGetFulfillmentOptionsPipeline _getOptions;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Sitecore.Commerce.Plugin.Fulfillment.FilterCartLineFulfillmentOptionsBlock" /> class.
        /// </summary>
        /// <param name="getOptionsPipeline">
        /// The get  fulfillment options pipeline
        /// </param>
        public FilterCartLineFulfillmentOptionsExtendedBlock(IGetFulfillmentOptionsPipeline getOptionsPipeline)
          : base((string)null)
        {
            this._getOptions = getOptionsPipeline;
        }

        /// <summary>The execute.</summary>
        /// <param name="arg">The argument.</param>
        /// <param name="context">The context.</param>
        /// <returns>The list of available fulfillment options.</returns>
        public override async Task<IEnumerable<FulfillmentOption>> Run(
          CartLineArgument arg,
          CommercePipelineExecutionContext context)
        {
            FilterCartLineFulfillmentOptionsExtendedBlock fulfillmentOptionsBlock = this;
            Condition.Requires<CartLineArgument>(arg).IsNotNull<CartLineArgument>("The arg can not be null");
            Condition.Requires<Cart>(arg.Cart).IsNotNull<Cart>("The cart can not be null");
            Condition.Requires<CartLineComponent>(arg.Line).IsNotNull<CartLineComponent>("The cart line can not be null");
            Cart cart = arg.Cart;
            if (!cart.Lines.Any<CartLineComponent>())
            {
                CommercePipelineExecutionContext executionContext = context;
                CommerceContext commerceContext = context.CommerceContext;
                string validationError = context.CommerceContext.GetPolicy<KnownResultCodes>().ValidationError;
                object[] args = new object[1] { (object)cart.Id };
                string defaultMessage = "Cart '" + cart.Id + "' has no lines";
                executionContext.Abort(await commerceContext.AddMessage(validationError, "CartHasNoLines", args, defaultMessage).ConfigureAwait(false), (object)context);
                executionContext = (CommercePipelineExecutionContext)null;
                return new List<FulfillmentOption>().AsEnumerable<FulfillmentOption>();
            }
            List<FulfillmentOption> list = (await fulfillmentOptionsBlock._getOptions.Run(string.Empty, context).ConfigureAwait(false)).ToList<FulfillmentOption>();
            if (list.Any<FulfillmentOption>())
            {
                FulfillmentOption fulfillmentOption = list.FirstOrDefault<FulfillmentOption>((Func<FulfillmentOption, bool>)(o => o.FulfillmentType.Equals("SplitShipping", StringComparison.OrdinalIgnoreCase)));
                if (fulfillmentOption != null)
                    list.Remove(fulfillmentOption);
            }
            CartLineComponent cartLineComponent = cart.Lines.WithSubLines().FirstOrDefault<CartLineComponent>((Func<CartLineComponent, bool>)(p => string.Equals(p.Id, arg.Line.Id, StringComparison.Ordinal)));
            if (cartLineComponent == null)
            {
                // ISSUE: explicit non-virtual call
                context.Logger.LogError((fulfillmentOptionsBlock.Name) + "-CartLineDoesNotExist: Cart=" + cart.Id + "|Line=" + arg.Line.Id);
            }
            else if (cartLineComponent.GetComponent<CartProductComponent>().Tags.Any<Tag>((Func<Tag, bool>)(p => p.Name.Equals("entitlement", StringComparison.OrdinalIgnoreCase))))
            {
                if (list.Any<FulfillmentOption>((Func<FulfillmentOption, bool>)(p => p.FulfillmentType.Equals("ShipToMe", StringComparison.OrdinalIgnoreCase))))
                    list.Remove(list.First<FulfillmentOption>((Func<FulfillmentOption, bool>)(p => p.FulfillmentType.Equals("ShipToMe", StringComparison.OrdinalIgnoreCase))));
            }
            else if (list.Any<FulfillmentOption>((Func<FulfillmentOption, bool>)(p => p.FulfillmentType.Equals("Digital", StringComparison.OrdinalIgnoreCase))))
                list.Remove(list.First<FulfillmentOption>((Func<FulfillmentOption, bool>)(p => p.FulfillmentType.Equals("Digital", StringComparison.OrdinalIgnoreCase))));
            return list.AsEnumerable<FulfillmentOption>();
        }
    }
}
