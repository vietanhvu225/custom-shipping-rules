﻿using Sitecore.Commerce.Core;
using Sitecore.Commerce.Plugin.Availability;
using Sitecore.Commerce.Plugin.Carts;
using Sitecore.Commerce.Plugin.Fulfillment;
using Sitecore.Framework.Conditions;
using Sitecore.Framework.Pipelines;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Feature.Shipping.Engine.Pipelines.Blocks
{
    [PipelineDisplayName("Fulfillment.block.FilterCartFulfillmentOptionsBlock")]
    public class FilterCartFulfillmentOptionsExtendedBlock : PipelineBlock<CartArgument, IEnumerable<FulfillmentOption>, CommercePipelineExecutionContext>
    {
        private readonly IGetFulfillmentOptionsPipeline _getOptions;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Sitecore.Commerce.Plugin.Fulfillment.FilterCartFulfillmentOptionsBlock" /> class.
        /// </summary>
        /// <param name="getOptionsPipeline">
        /// The get  fulfillment options pipeline
        /// </param>
        public FilterCartFulfillmentOptionsExtendedBlock(IGetFulfillmentOptionsPipeline getOptionsPipeline)
          : base((string)null)
        {
            this._getOptions = getOptionsPipeline;
        }

        /// <summary>The execute.</summary>
        /// <param name="arg">The argument.</param>
        /// <param name="context">The context.</param>
        /// <returns>The list of available fulfillment options.</returns>
        public override async Task<IEnumerable<FulfillmentOption>> Run(
          CartArgument arg,
          CommercePipelineExecutionContext context)
        {
            Condition.Requires<CartArgument>(arg).IsNotNull<CartArgument>("The arg can not be null");
            Condition.Requires<Cart>(arg.Cart).IsNotNull<Cart>("The cart can not be null");
            Cart cart = arg.Cart;
            if (!cart.Lines.Any<CartLineComponent>())
            {
                CommercePipelineExecutionContext executionContext = context;
                CommerceContext commerceContext = context.CommerceContext;
                string validationError = context.CommerceContext.GetPolicy<KnownResultCodes>().ValidationError;
                object[] args = new object[1] { (object)cart.Id };
                string defaultMessage = "Cart '" + cart.Id + "' has no lines";
                executionContext.Abort(await commerceContext.AddMessage(validationError, "CartHasNoLines", args, defaultMessage).ConfigureAwait(false), (object)context);
                executionContext = (CommercePipelineExecutionContext)null;
                return new List<FulfillmentOption>().AsEnumerable<FulfillmentOption>();
            }
            List<FulfillmentOption> list = (await this._getOptions.Run(string.Empty, context).ConfigureAwait(false)).ToList<FulfillmentOption>();
            if (list.Any<FulfillmentOption>() && cart.Lines.Count == 1)
            {
                FulfillmentOption fulfillmentOption = list.FirstOrDefault<FulfillmentOption>((Func<FulfillmentOption, bool>)(o => o.FulfillmentType.Equals("SplitShipping", StringComparison.OrdinalIgnoreCase)));
                if (fulfillmentOption != null)
                    list.Remove(fulfillmentOption);
            }
            foreach (CartLineComponent withSubLine in (IEnumerable<CartLineComponent>)arg.Cart.Lines.WithSubLines())
            {
                if (!withSubLine.CartSubLineComponents.Any<CartLineComponent>())
                {
                    if (withSubLine.GetComponent<CartProductComponent>().HasPolicy<AvailabilityAlwaysPolicy>())
                    {
                        if (list.Any<FulfillmentOption>((Func<FulfillmentOption, bool>)(p => p.FulfillmentType.Equals("ShipToMe", StringComparison.OrdinalIgnoreCase))))
                            list.Remove(list.First<FulfillmentOption>((Func<FulfillmentOption, bool>)(p => p.FulfillmentType.Equals("ShipToMe", StringComparison.OrdinalIgnoreCase))));
                    }
                    else if (list.Any<FulfillmentOption>((Func<FulfillmentOption, bool>)(p => p.FulfillmentType.Equals("Digital", StringComparison.OrdinalIgnoreCase))))
                        list.Remove(list.First<FulfillmentOption>((Func<FulfillmentOption, bool>)(p => p.FulfillmentType.Equals("Digital", StringComparison.OrdinalIgnoreCase))));
                }
            }
            return list.AsEnumerable<FulfillmentOption>();
        }
    }
}
