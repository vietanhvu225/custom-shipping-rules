﻿using Feature.Shipping.Engine.Helpers;
using Feature.Shipping.Engine.Models;
using Feature.Shipping.Engine.Policies;
using Microsoft.Extensions.Logging;
using Sitecore.Commerce.Core;
using Sitecore.Commerce.Plugin.Carts;
using Sitecore.Commerce.Plugin.Fulfillment;
using Sitecore.Commerce.Plugin.Models;
using Sitecore.Commerce.Plugin.Pricing;
//using Sitecore.Data;
using Sitecore.Framework.Conditions;
using Sitecore.Framework.Pipelines;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Feature.Shipping.Engine.Pipelines.Blocks
{
    [PipelineDisplayName("Fulfillment.block.CalculateCartLinesFulfillmentExtendedBlock")]
    public class CalculateCartLinesFulfillmentExtendedBlock : PipelineBlock<Cart, Cart, CommercePipelineExecutionContext>
    {
        public CalculateCartLinesFulfillmentExtendedBlock()
        {

        }
        public async override Task<Cart> Run(Cart arg, CommercePipelineExecutionContext context)
        {
            Condition.Requires<Cart>(arg).IsNotNull<Cart>(this.Name + ": The cart cannot be null.");
            Condition.Requires<IList<CartLineComponent>>(arg.Lines).IsNotNull<IList<CartLineComponent>>(this.Name + ": The cart lines cannot be null.");
            if (!arg.Lines.Any<CartLineComponent>())
                await Task.FromResult<Cart>(arg);
            List<CartLineComponent> list = arg.Lines.Where<CartLineComponent>((Func<CartLineComponent, bool>)(line =>
            {
                if (line != null)
                    return line.HasComponent<FulfillmentComponent>();
                return false;
            })).Select<CartLineComponent, CartLineComponent>((Func<CartLineComponent, CartLineComponent>)(l => l)).ToList<CartLineComponent>();
            if (!list.Any<CartLineComponent>())
            {
                ((DefaultPipelineExecutionContext)context).Logger.LogInformation(this.Name + " - No lines with fulfillment components");
                return arg;
            }
            string currency = context.CommerceContext.CurrentCurrency();
            GlobalPhysicalFulfillmentPolicy policy = (GlobalPhysicalFulfillmentPolicy)context.GetPolicy<GlobalPhysicalFulfillmentPolicy>();

            Decimal amount = policy.DefaultItemFulfillmentFee.Amount;
            if (((IEnumerable<Money>)policy.DefaultItemFulfillmentFees).Any<Money>((Func<Money, bool>)(p => p.CurrencyCode.Equals(currency, StringComparison.OrdinalIgnoreCase))))
                amount = ((IEnumerable<Money>)policy.DefaultItemFulfillmentFees).First<Money>((Func<Money, bool>)(p => p.CurrencyCode.Equals(currency, StringComparison.OrdinalIgnoreCase))).Amount;
            ((DefaultPipelineExecutionContext)context).Logger.LogInformation(string.Format("{0} - Default Fulfillment Fee:{1} {2}", (object)this.Name, (object)currency, (object)amount));

            // Get fulfillment settings
            var fulfillmentSettingsModel = new FulfillmentSettingsModel();
            await FulfillmentHelpers.GetFulfillmentSettings(fulfillmentSettingsModel, context.CommerceContext);
            decimal allBillAmount = 0;
            allBillAmount = arg.Lines.Select(t => t.Totals).Select(s => s.SubTotal).Select(a => a.Amount).Sum();

            foreach (CartLineComponent cartLineComponent in list)
            {

                FulfillmentComponent lineFulfillmentComponent = ((IEnumerable)cartLineComponent.ChildComponents).OfType<FulfillmentComponent>().FirstOrDefault<FulfillmentComponent>();
                if (!(lineFulfillmentComponent is ElectronicFulfillmentComponent))
                {
                    ((DefaultPipelineExecutionContext)context).Logger.LogInformation(string.Format("{0} - Adding fulfillment fee to line:{1} {2}", (object)this.Name, (object)currency, (object)amount));



                    if (policy.FulfillmentFees.Any<FulfillmentFee>((Func<FulfillmentFee, bool>)(p =>
                    {
                        if (p.Name.Equals(lineFulfillmentComponent?.FulfillmentMethod.Name, StringComparison.OrdinalIgnoreCase))
                            return p.Fee.CurrencyCode.Equals(context.CommerceContext.CurrentCurrency(), StringComparison.OrdinalIgnoreCase);
                        return false;
                    })))
                    {
                        amount = policy.FulfillmentFees.First<FulfillmentFee>((Func<FulfillmentFee, bool>)(p => p.Name.Equals(lineFulfillmentComponent?.FulfillmentMethod.Name, StringComparison.OrdinalIgnoreCase))).Fee.Amount;
                        ((DefaultPipelineExecutionContext)context).Logger.LogInformation(this.Name + " - Specific fee:" + lineFulfillmentComponent?.FulfillmentMethod.Name);
                    }
                    else
                    {
                        ((DefaultPipelineExecutionContext)context).Logger.LogInformation(this.Name + " - No specific fee:" + lineFulfillmentComponent?.FulfillmentMethod.Name);
                    }                    

                    decimal currentCartPrice = cartLineComponent.UnitListPrice.Amount;
                    CustomerFulfillmentInformationModel customerFulfillmentInformationModel = new CustomerFulfillmentInformationModel();
                    FulfillmentHelpers.GetCustomerFulfillmentInformation(customerFulfillmentInformationModel, (PhysicalFulfillmentComponent)lineFulfillmentComponent);

                    // Calculate fulfillment amount
                    if (fulfillmentSettingsModel.SubdivisionFreeShipping.Contains(customerFulfillmentInformationModel.StateCode)
                        || currentCartPrice > Convert.ToDecimal(fulfillmentSettingsModel.FreeShippingForTotalAmount)
                        || allBillAmount > Convert.ToDecimal(fulfillmentSettingsModel.FreeShippingForTotalAmount)
                        || cartLineComponent.HasPolicy<FreeShippingItemTagsPolicy>())
                    {
                        amount = 0;
                    }
                    else if (!string.IsNullOrWhiteSpace(fulfillmentSettingsModel.FixedMinFee))
                    {
                        var fixedMinFeeCollection = HttpUtility.ParseQueryString(fulfillmentSettingsModel.FixedMinFee);
                        foreach (string key in fixedMinFeeCollection)
                        {
                            var fixShippingAmount = fixedMinFeeCollection[key];
                            if (currentCartPrice < Convert.ToDecimal(key))
                            {
                                amount = Convert.ToDecimal(fixShippingAmount);
                                break;
                            }
                        }
                    }
                    else if (cartLineComponent.HasPolicy<SmallProductTagsPolicy>())
                    {
                        amount = Convert.ToDecimal(fulfillmentSettingsModel.SmallProductsShippingCost);
                    }

                    IList<AwardedAdjustment> adjustments = cartLineComponent.Adjustments;
                    CartLineLevelAwardedAdjustment awardedAdjustment = new CartLineLevelAwardedAdjustment();
                    ((Adjustment)awardedAdjustment).Name = "FulfillmentFee";
                    ((Adjustment)awardedAdjustment).DisplayName = "FulfillmentFee";
                    ((AwardedAdjustment)awardedAdjustment).Adjustment = new Money(currency, amount);
                    ((AwardedAdjustment)awardedAdjustment).AdjustmentType = ((KnownCartAdjustmentTypesPolicy)context.GetPolicy<KnownCartAdjustmentTypesPolicy>()).Fulfillment;
                    ((AwardedAdjustment)awardedAdjustment).IsTaxable = false;
                    ((AwardedAdjustment)awardedAdjustment).AwardingBlock = this.Name;
                    ((AwardedAdjustment)awardedAdjustment).IncludeInGrandTotal = false;
                    ((ICollection<AwardedAdjustment>)adjustments).Add((AwardedAdjustment)awardedAdjustment);

                    context.Logger.LogInformation($"{this.Name}FulfillmentFeeLines for current Line {list.IndexOf(cartLineComponent)}is {awardedAdjustment.Adjustment.Amount}");
                }
                
            }

            return arg;
        }
    }
}
