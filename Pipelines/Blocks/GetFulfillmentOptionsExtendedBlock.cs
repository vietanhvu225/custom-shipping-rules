﻿using Sitecore.Commerce.Core;
using Sitecore.Commerce.Plugin.Fulfillment;
using Sitecore.Commerce.Plugin.Management;
using Sitecore.Framework.Caching;
using Sitecore.Framework.Conditions;
using Sitecore.Framework.Pipelines;
using Sitecore.Services.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Feature.Shipping.Engine.Pipelines.Blocks
{
    [PipelineDisplayName("Management.block.getfulfillmentoptionsextended")]
    public class GetFulfillmentOptionsExtendedBlock : BaseFulfillmentBlock<IEnumerable<FulfillmentOption>, IEnumerable<FulfillmentOption>>
    {
        public GetFulfillmentOptionsExtendedBlock(CommerceCommander commander)
          : base(commander)
        {
        }

        public override async Task<IEnumerable<FulfillmentOption>> Run(
          IEnumerable<FulfillmentOption> arg,
          CommercePipelineExecutionContext context)
        {
            GetFulfillmentOptionsExtendedBlock fulfillmentOptionsBlock = this;
            List<FulfillmentOption> source = arg as List<FulfillmentOption> ?? arg.ToList<FulfillmentOption>();
            // ISSUE: explicit non-virtual call
            Condition.Requires<List<FulfillmentOption>>(source).IsNotNull<List<FulfillmentOption>>((fulfillmentOptionsBlock.Name) + ": argument cannot be null.");
            if (source.Any<FulfillmentOption>())
                return (IEnumerable<FulfillmentOption>)source;
            string currentShopName = context.CommerceContext.CurrentShopName();
            string cacheKey = "fulfillmentoptions|" + currentShopName + "|" + context.CommerceContext.CurrentLanguage();
            ManagementCachePolicy cachePolicy = context.GetPolicy<ManagementCachePolicy>();
            List<ItemModel> items;
            if (cachePolicy.AllowCaching)
            {
                items = await fulfillmentOptionsBlock.Commander.GetCacheEntry<List<ItemModel>>(context.CommerceContext, cachePolicy.ItemsCollectionCacheName, cacheKey).ConfigureAwait(false);
                if (items != null)
                {
                    context.CommerceContext.AddUniqueObjectByType((object)new KeyValuePair<string, List<ItemModel>>(cacheKey, items));
                    return Enumerable.Empty<FulfillmentOption>();
                }
            }
            SitecoreControlPanelItemsPolicy itemsPolicy = context.GetPolicy<SitecoreControlPanelItemsPolicy>();
            string str1 = context.CommerceContext.GetObject<KeyValuePair<string, string>>((Func<KeyValuePair<string, string>, bool>)(kv => kv.Key.Equals("itemPath", StringComparison.OrdinalIgnoreCase))).Value ?? string.Empty;
            string itemPath = string.IsNullOrEmpty(str1) ? itemsPolicy?.StorefrontsPath + "/" + currentShopName : str1;
            items = new List<ItemModel>();
            if (await fulfillmentOptionsBlock.Commander.Pipeline<IGetItemByPathPipeline>().Run(new ItemModelArgument(itemPath), context).ConfigureAwait(false) != null)
            {
                IEnumerable<ItemModel> storefrontConfigurationItems = await fulfillmentOptionsBlock.Commander.Pipeline<IGetItemsByPathPipeline>().Run(new ItemModelArgument(itemPath), context).ConfigureAwait(false);
                ItemModel itemModel1 = await fulfillmentOptionsBlock.GetFulfillmentConfigurationItem(itemPath, storefrontConfigurationItems, itemsPolicy, context).ConfigureAwait(false);
                if (itemModel1 != null)
                {
                    SitecoreItemFieldsPolicy policy = context.GetPolicy<SitecoreItemFieldsPolicy>();
                    string str2 = itemModel1.ContainsKey(policy.FulfillmentOptions) ? itemModel1[policy.FulfillmentOptions] as string : string.Empty;
                    if (!string.IsNullOrEmpty(str2))
                    {
                        string[] strArray1 = str2.Split('|');
                        if (((IEnumerable<string>)strArray1).Any<string>())
                        {
                            string[] strArray = strArray1;
                            for (int index = 0; index < strArray.Length; ++index)
                            {
                                string itemPathOrId = strArray[index];
                                ItemModel itemModel2 = await fulfillmentOptionsBlock.Commander.Pipeline<IGetItemByIdPipeline>().Run(new ItemModelArgument(itemPathOrId), context).ConfigureAwait(false);
                                if (itemModel2 != null)
                                    items.Add(itemModel2);
                            }
                            strArray = (string[])null;
                            if (cachePolicy.AllowCaching)
                            {
                                int num = await fulfillmentOptionsBlock.Commander.SetCacheEntry<List<ItemModel>>(context.CommerceContext, cachePolicy.ItemsCollectionCacheName, cacheKey, (ICachable)new Cachable<List<ItemModel>>(items, 1L), cachePolicy.GetCacheEntryOptions()).ConfigureAwait(false) ? 1 : 0;
                            }
                            context.CommerceContext.AddUniqueObjectByType((object)new KeyValuePair<string, List<ItemModel>>(cacheKey, items));
                            return Enumerable.Empty<FulfillmentOption>();
                        }
                    }
                }
            }
            CommercePipelineExecutionContext executionContext = context;
            CommerceContext commerceContext = context.CommerceContext;
            string warning = context.GetPolicy<KnownResultCodes>().Warning;
            object[] args = new object[1] { (object)itemPath };
            string defaultMessage = "Fulfillment options not found for Storefront " + itemPath + ".";
            executionContext.Abort(await commerceContext.AddMessage(warning, "EntityNotFound", args, defaultMessage).ConfigureAwait(false), (object)context);
            executionContext = (CommercePipelineExecutionContext)null;
            return Enumerable.Empty<FulfillmentOption>();
        }
    }
}
